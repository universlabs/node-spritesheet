path = require( "path" )

class Style
  LESS = 
    classWrapper: null
    classInner: null
    mediaQueries: []
    spriteSelectors: []

  @generateLess: ()->
    inner = "#{LESS.classInner}\n"
    inner += LESS.mediaQueries.join('')
    inner += LESS.spriteSelectors.join('')

    less = LESS.classWrapper + " {\n#{inner}\n}"

    return less

  constructor: ( options ) ->
    @selector = options.selector
    @pixelRatio = options.pixelRatio || 1
    
    @resolveImageSelector = options.resolveImageSelector if options.resolveImageSelector

  css: ( selector, attributes ) ->
    "#{ selector } {\n#{ @cssStyle( attributes ) };\n    }\n"
  
  cssStyle: ( attributes, indent ) ->
    if indent
      attributes.join ";\n    "
    else  
      attributes.join ";\n"
  
  cssComment: ( comment ) ->
    "/*\n#{ comment }\n*/"
  
  resolveImageSelector: ( name ) ->
    name
  
  generate: ( options ) ->
    { imagePath, relativeImagePath, images, pixelRatio, width, height } = options
    relativeImagePath = relativeImagePath.replace /(\\+)/g, "/"
    @pixelRatio = pixelRatio || 1

    attrs = [
        "    background-image: url( '#{ relativeImagePath }' )"
        "    background-repeat: no-repeat"
        "    background-size: #{ width / pixelRatio }px #{ height / pixelRatio }px"
      ]


    # Only add background-position, width and height for pixelRatio === 1.
    if pixelRatio is 1
      LESS.classWrapper = @selector
      LESS.classInner = @cssStyle( attrs ) + ';'

      for image in images
        positionX = ( -image.cssx / pixelRatio )
        if positionX != 0
          positionX = positionX+'px'

        positionY = ( -image.cssy / pixelRatio )
        if positionY != 0
          positionY = positionY+'px'

        attr = [
          "        width: #{ image.cssw / pixelRatio }px"
          "        height: #{ image.cssh / pixelRatio }px"
          "        background-position: #{positionX} #{positionY}"
        ]


        image.style = @cssStyle attr
        image.selector = @resolveImageSelector( image.name, image.path )

        LESS.spriteSelectors.push(@css( [ '    &', image.selector ].join( '.' ), attr ))
    
    if pixelRatio > 1
      style = @cssStyle( attrs, true ) + ';'
      LESS.mediaQueries.push(@wrapMediaQuery( style ))
  
    return 'null string - see style.coffee'
  
  comment: ( comment ) ->
    @cssComment comment
    
  wrapMediaQuery: ( css ) ->
    "    @media (min--moz-device-pixel-ratio: #{ @pixelRatio }),(-o-min-device-pixel-ratio: #{ @pixelRatio }/1),(-webkit-min-device-pixel-ratio: #{ @pixelRatio }),(min-device-pixel-ratio: #{ @pixelRatio }) {\n
    #{ css }\n
    }\n"
  
module.exports = Style
